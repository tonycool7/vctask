<?php

use App\Http\Controllers\{
    AdvertController
};

//We can send an callback as a second parameter

//$app->router->get('/', function(){
//    $renderer = new Renderer();
//
//    return $renderer->renderView('home');
//});


//We can also just send the name of the view as a second parameter
//$app->router->get('/', 'home');

//we can also send an array of two elements, where the first
// element is a controller and the second is an action

$app->router->get('/', [AdvertController::class, "home"]);

$app->router->get('/advert', [AdvertController::class, "index"]);

