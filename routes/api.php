<?php
    use App\Http\Controllers\{
        AdvertController
    };

$app->router->get('/api/advert', [AdvertController::class, "getAll"]);

$app->router->get('/api/get-top-ad', [AdvertController::class, "getTopAdvert"]);

$app->router->get('/advert/{id}', [AdvertController::class, "edit"]);

$app->router->post('/api/advert', [AdvertController::class, "store"]);

$app->router->put('/api/advert', [AdvertController::class, "update"]);

$app->router->delete('/api/advert', [AdvertController::class, "delete"]);