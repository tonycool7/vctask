<?php

require_once 'vendor/autoload.php';

use Dotenv\Dotenv;

$dotenv = Dotenv::createImmutable(__DIR__ );

$dotenv->safeLoad();

use App\Core\{
    Application
};

$app = new Application();

$migrations = scandir(__DIR__.'/migrations');

$db = \App\Database\DbConnection::getConnection();

foreach ($migrations as $migration){
    if($migration == "." || $migration == "..") continue;

    $pathDetails = pathinfo($migration);

    $basename = $pathDetails['basename'];

    require_once __DIR__."/migrations/$basename";

    $instance = new $pathDetails['filename'];

    try {
        $createTable = $db->exec($instance->up());
        echo "Migration successful!\n";
    } catch (\PDOException $e) {
        exit($e->getMessage());
    }
}