# Как использовать API

## 0. Миграция
запустите скрипт composer install, Затем 
отредактируйте файл .env (Я предполагаю поместить это в файл .gitignore), затем запустите скрипт php migration.php в корневом каталоге проекта.

## 1. Создание объявления
curl -X POST \
http://localhost:8000/api/advert \
-H 'Accept: application/json' \
-H 'Content-Type: application/x-www-form-urlencoded' \
-H 'content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \
-F 'text=Buy cheap clothes' \
-F price=1400 \
-F amount=100 \
-F id=1 

## 2. Update advert
curl -X POST \
http://localhost:8000/api/advert \
-H 'Accept: application/json' \
-H 'Content-Type: application/x-www-form-urlencoded' \
-H 'content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \
-F 'text=Buy the latest smart bags' \
-F price=1900 \
-F amount=100 \
-F id=1 \
-F _method=put

## 3. Retrieve all adverts
curl -X GET \
http://localhost:8000/api/advert \
-H 'Accept: application/json' 

## 4. Открутка объявления 
curl -X GET \
http://localhost:8000/api/get-top-ad \
-H 'Accept: application/json' 