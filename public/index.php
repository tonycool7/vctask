<?php

require_once '../vendor/autoload.php';

use Dotenv\Dotenv;

$dotenv = Dotenv::createImmutable(__DIR__."/..");

$dotenv->safeLoad();

use App\Core\Application;

$app = new Application();

//web routes
include_once __DIR__ . "/../routes/web.php";


//api routes
include_once __DIR__ . "/../routes/api.php";

try{
    $app->run();
}catch (Exception $e){
    var_dump($e);
}
