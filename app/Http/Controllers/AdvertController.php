<?php

namespace App\Http\Controllers;

use App\Core\Model;
use App\Core\Request;
use App\Model\AdvertModel;

class AdvertController extends BaseController
{
    public Model $model;

    public function __construct(){
        $this->model = new AdvertModel();

        parent::__construct();
    }
    public function home(Request $request){
        $data = $this->model->getAll();

        return $this->render('home', ['adverts' => $data]);
    }

    public function index(Request $request){
        return $this->render('advert', ['name' => "Anthony"]);
    }

    protected function addRandomSrting($length) : string{
        return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
    }

    public function getAll(){
        $data = $this->model->getAll();

        return $this->response->json($data, 200);
    }

    public function show($id){
        $data = $this->model->getById($id);

        return $this->response->json($data, 200);
    }

    public function getTopAdvert(){
        $data = $this->model->selectTopAdvert();

        $url = $_ENV['SITE_URL'];

        $image = $data[0]['banner'];

        $data[0]['image'] = "$url/storage/$image";

        return $this->response->json($data, 200);
    }

    public function store(Request $request){
        $payload = $request->getBody();

        $data = [];

        $payload['banner'] = $this->addRandomSrting(8).".jpg";

        if($request->file('banner') && $request->file('banner')->saveFile($payload['banner'])){
            $data = $this->model->create($payload);
        }

        return $this->response->json($data, 201);
    }

    public function update(Request $request){
        $payload = $request->getBody();

        unset($payload['_method']);

        $data = [];

        if($request->file('banner') && $request->file('banner')->saveFile($payload['banner'])){
            $payload['banner'] = $this->addRandomSrting(8).".jpg";
        }

        $data = $this->model->update($payload);

        return $this->response->json($data, 202);
    }

    public function delete(){
        return $this->response->json("deleting advert", 201);
    }
}