<?php


namespace App\Http\Controllers;

use App\Core\{
    Response,
    Renderer
};

abstract class BaseController
{
    public Response $response;

    public Renderer $renderer;

    public function __construct()
    {
        $this->renderer = new Renderer();
        $this->response = new Response();
    }

    public function render($view, $params = []) : string{
        return $this->renderer->renderView($view, $params);
    }
}