<?php


namespace App\Database;


class DbOperations
{
    private $connection;

    public function __construct()
    {
        $this->connection = DbConnection::getConnection();
    }

    public function executePreparedQuery($statement, $values = []) : array{
        $result = [];

        try {
            $stmt = $this->connection->prepare($statement);
            $stmt->execute($values);
            $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }

        return $result;
    }

    public function read($statement){
        $result = [];

        try {
            $result = $this->connection->query($statement)->fetchAll(\PDO::FETCH_ASSOC);
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }

        return $result;
    }

    public function fetchAll($table){
        $query = "select * from $table";

        return $this->read($query);
    }

    public function fetchById($table, $id){
        $query = "select * from $table where id = :id";

        return $this->executePreparedQuery($query, ['id' => $id]);
    }

    public function insertRow($table, $attributes, $values) : array{

        $attributesStr = implode(', ', $attributes);

        array_walk($attributes, function(&$value, $key){
           $value = ":$value";
        });

        $attributePlacholders = implode(', ', $attributes);

        $query = "insert into $table ($attributesStr) values ($attributePlacholders)";

        $this->executePreparedQuery($query, $values);

        return $this->fetchById($table, (int)$this->connection->lastInsertId());
    }

    public function updateTable($table, $attributes, $values) : array{

        $valueClone = $values;

        unset($values['id']);

        array_walk($values, function(&$value, $key){
            $value = "$key=:$key";
        });

        $attributePlacholders = implode(', ', $values);

        $query = "UPDATE $table SET $attributePlacholders WHERE id=:id";

        $this->executePreparedQuery($query, $valueClone);

        return $this->fetchById($table, $valueClone['id']);
    }

    public function deleteRow($table, $rowId) : bool{

    }
}