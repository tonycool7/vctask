<?php

namespace App\Database;

class DbConnection
{
    private static $dbConnection;

    private function __construct(){}

    public static function getConnection(){

        if(self::$dbConnection === null){

            $connection = $_ENV["DB_CONNECTION"];
            $db = $_ENV["DB_DATABASE"];
            $password = $_ENV["DB_PASSWORD"];
            $host = $_ENV["DB_HOST"];
            $username = $_ENV["DB_USERNAME"];

            try{
                self::$dbConnection = new \PDO("{$connection}:dbname={$db};host={$host};charset=utf8", $username, $password);

                self::$dbConnection->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);

                self::$dbConnection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            }catch (\Exception $e){
                var_dump($e);
            }
        }

        return self::$dbConnection;
    }

}