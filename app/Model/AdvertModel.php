<?php


namespace App\Model;

use App\Core\Model;

class AdvertModel extends Model
{
    protected string $table = "advert";

    protected array $fillable = [
        "text", "price", "amount", "banner"
    ];

    public function selectTopAdvert(){
        $query = "SELECT text, banner from $this->table WHERE price=(SELECT MAX(price) FROM $this->table) AND views < amount";

        return $this->db->read($query);
    }

}