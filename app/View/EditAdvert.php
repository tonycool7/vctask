<h1>Edit Advert!</h1>

<h5><strong>Edit an advert</strong></h5>

<form enctype="multipart/form-data" method="put" action="/api/advert">

    <input type="hidden" name="id" value="<?php echo $id; ?>" />

    <div class="form-group">
        <label>Text</label>
        <input name="text" placeholder="Advert announcement" value="<?php echo $text; ?>" class="form-control" type="text" required/>
    </div>

    <div class="form-group">
        <label>Price</label>
        <input name="price" placeholder="Advert price" value="<?php echo $price; ?>" class="form-control" type="number" required />
    </div>

    <div class="form-group">
        <label>Display Limit</label>
        <input name="amount" placeholder="Advert display limit" value="<?php echo $amount; ?>" class="form-control" type="number" required />
    </div>

    <div class="form-group">
        <label>Advert Banner Image</label>
        <input type="file" class="form-control" name="banner"  />
    </div>

    <div class="form-group">
        <button class="btn btn-primary">Update Advert</button>
    </div>
</form>