<h1>Welcome to this Advert placement app!</h1>
<p>Let place some adverts</p>

<table class="table">
    <thead>
        <tr>
            <th>text</th>
            <th>price</th>
            <th>amount</th>
            <th>banner</th>
        </tr>
    </thead>
    <tbody>
        <?php

        foreach ($adverts as $advert){
            echo "<tr>";
            echo "<td>{$advert['text']}</td>";
            echo "<td>{$advert['price']}</td>";
            echo "<td>{$advert['amount']}</td>";
            echo "<td>{$advert['banner']}</td>";
            echo "</tr>";
        }

        ?>
    </tbody>
</table>
