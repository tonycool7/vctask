<h1>Welcome to this Advert placement app!</h1>
<h3>Welcome <?php echo $name; ?></h3>

<h5><strong>Create an advert</strong></h5>

<form enctype="multipart/form-data" method="post" action="/api/advert">

    <div class="form-group">
        <label>Text</label>
        <input name="text" placeholder="Advert announcement" class="form-control" type="text" required/>
    </div>

    <div class="form-group">
        <label>Price</label>
        <input name="price" placeholder="Advert price" class="form-control" type="number" required />
    </div>

    <div class="form-group">
        <label>Display Limit</label>
        <input name="amount" placeholder="Advert display limit" class="form-control" type="number" required />
    </div>

    <div class="form-group">
        <label>Advert Banner Image</label>
        <input type="file" class="form-control" name="banner"  />
    </div>

    <div class="form-group">
        <button class="btn btn-primary">Create Advert</button>
    </div>
</form>