<?php


namespace App\Core;

class Router
{
    private array $routes = [];

    private array $allowedMethods = ['get', 'post'];

    private Request $request;

    private Renderer $renderer;

    private Response $response;

    public function __construct(Request $request, Renderer $renderer, Response $response){
        $this->request = $request;
        $this->renderer = $renderer;
        $this->response = $response;
    }

    public function get($path, $callback) : void{
        $this->routes['get'][$path] = $callback;
    }

    public function post($path, $callback) : void{
        $this->routes['post'][$path] = $callback;
    }

    public function put($path, $callback) : void{
        $this->routes['put'][$path] = $callback;
    }

    public function delete($path, $callback) : void{
        $this->routes['delete'][$path] = $callback;
    }

    public function resolve(){
        $path = $this->request->getPath();

        $_method = in_array("_method", array_keys($this->request->getBody())) ? $this->request->getBody()['_method'] : null;

        $method = $this->request->getMethod();

        if(!in_array($method, $this->allowedMethods)) {
            $this->response->setStatusCode(403);

            return "Method not allowed";
        }

        $callback = $this->routes[($_method) ? $_method : $method][$path] ?? false;

        if(!$callback){
            $this->response->setStatusCode(404);

            return "Not Found";
        }

        if(is_string($callback)){
            return $this->renderer->renderView($callback);
        }

        if(is_array($callback) && count($callback) == 2){
            $callback[0] = new $callback[0];
        }

        return call_user_func($callback, $this->request);
    }

}