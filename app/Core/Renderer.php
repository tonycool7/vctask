<?php


namespace App\Core;


class Renderer
{
    public function __construct()
    {
    }

    public function getView($view, $params = []) : string
    {
        foreach ($params as $key => $value){
            $$key = $value;
        }

        ob_start();
        include_once __DIR__."/../View/{$view}.php";
        return ob_get_clean();
    }

    public function getLayout() : string{
        ob_start();
        include_once __DIR__ . "/../View/Layout.php";
        return ob_get_clean();
    }

    public function renderView($view, $params = []) : string{
        $layoutContent = $this->getLayout();
        $viewContent = $this->getView($view, $params);

        return str_replace("{{content}}", $viewContent, $layoutContent);
    }
}