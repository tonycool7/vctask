<?php


namespace App\Core;


use App\Database\DbConnection;
use App\Database\DbOperations;

//Parent class for model, It has all the basic functions all child models will need
class Model
{
    protected string $table;

    protected array $fillable;

    protected $db;

    public function __construct()
    {
        $this->db = new DbOperations();
    }

    public function create($values) : array{
        return $this->db->insertRow($this->table, $this->fillable, $values);
    }

    public function getAll(){
        return $this->db->fetchAll($this->table);
    }

    public function getById($id){
        return $this->db->fetchById($this->table, $id);
    }

    public function update($values) : array{
        return $this->db->updateTable($this->table, $this->fillable, $values);
    }

    public function delete($id) : bool{
        return $this->db->deleteRow($this->table,$id);
    }
}