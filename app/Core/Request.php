<?php


namespace App\Core;


class Request
{
    use UploadFile;

    public function __construct()
    {
    }

    public function cleanInput($name){
        $type = $this->getMethod() === "post" ? INPUT_POST : INPUT_GET;

        return filter_input($type, $name, FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    }

    public function getBody(){
        $body = [];

        foreach($_REQUEST as $key => $value){
            $body[$key] = strip_tags(html_entity_decode($this->cleanInput($key)));
        }

        return $body;
    }

    public function getPath() : string{
        return explode('?', $_SERVER['REQUEST_URI'])[0];
    }

    public function file($name){
        if(!array_key_exists($name, array_keys($_FILES))) return false;

        $this->setFile($_FILES[$name]);

        return $this;
    }

    public function getMethod() : string{
        return strtolower($_SERVER['REQUEST_METHOD']);
    }
}