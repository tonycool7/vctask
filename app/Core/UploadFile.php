<?php


namespace App\Core;

trait UploadFile
{
    public $file;

    public function saveFile($newName = ""){
        if (move_uploaded_file($this->getFilePath(), __DIR__."/../../public/storage/$newName")) {
            return true;
        } else {
            return false;
        }
    }

    public function setFile($file){
        $this->file = $file;
    }

    public function getFileName(){
        if(!$this->file) return false;

        return $this->file['name'];
    }

    public function getFileType(){
        if(!$this->file) return false;

        return $this->file['type'];
    }

    public function getFilePath(){
        if(!$this->file) return false;

        return $this->file['tmp_name'];
    }

    public function getFileSize(){
        if(!$this->file) return false;

        return $this->file['size'];
    }
}