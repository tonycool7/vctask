<?php


namespace App\Core;


class Response
{
    public function __construct(){

    }

    public function setStatusCode(int $code){
        http_response_code($code);
    }

    public function redirect($url){
        $siteUrl = $_ENV['SITE_URL'];
        header("Location: $siteUrl/$url");
    }

    public function json($data, $code = 200){

        header('Content-Type: application/json');

        $this->setStatusCode($code);

        return json_encode([
            'data' => $data
        ]);
    }
}