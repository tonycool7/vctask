<?php

namespace App\Core;

use App\Core\{
    Router,
    Request,
    Response
};

class Application
{

    public Router $router;

    public Request $request;

    public Renderer $renderer;

    public Response $response;

    public function __construct(){
        
        $this->request = new Request();

        $this->renderer = new Renderer();

        $this->response = new Response();

        $this->router = new Router($this->request, $this->renderer, $this->response);

    }

    public function run() : void{
        echo $this->router->resolve();
    }

}