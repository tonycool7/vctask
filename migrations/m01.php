<?php


class m01
{
    public function up(){
        return "CREATE TABLE IF NOT EXISTS advert (
                    id INT NOT NULL AUTO_INCREMENT,
                    text TEXT NOT NULL,
                    price DECIMAL(8,2) NOT NULL,
                    amount INT NOT NULL,
                    banner VARCHAR(150) NOT NULL,
                    views INT DEFAULT 0,
                    PRIMARY KEY (id)
                ) ENGINE=INNODB;";
    }

    public function down(){
        return "DROP TABLE advert";
    }

}